package com.izeno.demo.kafkaconsumer.mongo.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.izeno.demo.kafkaconsumer.mongo.model.TrxHist;

public interface TrxHistRepository extends MongoRepository<TrxHist, String> {

	public long count();
}
