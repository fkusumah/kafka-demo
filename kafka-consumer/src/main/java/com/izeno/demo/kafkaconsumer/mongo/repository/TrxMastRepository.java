package com.izeno.demo.kafkaconsumer.mongo.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.izeno.demo.kafkaconsumer.mongo.model.TrxMast;

public interface TrxMastRepository extends MongoRepository<TrxMast, String> {

	@Query("{acctno:'?0'}")
	TrxMast findTrxMastByAcctNo(String acctno);
	
	@Query("{cifno:'?0'}")
	TrxMast findTrxMastByCifNo(String cifno);
	
	public long count();
}
