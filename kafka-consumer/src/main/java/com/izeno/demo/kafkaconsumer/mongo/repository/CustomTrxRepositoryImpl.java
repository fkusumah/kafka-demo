package com.izeno.demo.kafkaconsumer.mongo.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Component;

import com.izeno.demo.kafkaconsumer.mongo.model.TrxMast;
import com.mongodb.DBObject;

@Component
public class CustomTrxRepositoryImpl implements CustomTrxRepository {

	@Autowired
	MongoTemplate mongoTemplate;
	
	@Override
	public void updateCbal(String acctno, int cbal) {
		Query query = new Query(Criteria.where("acctno").is(acctno));
		Update update = new Update();
		update.set("cbal", cbal);
		
		mongoTemplate.updateFirst(query, update, TrxMast.class);
		
		/*UpdateResult result = mongoTemplate.updateFirst(query, update, TrxMast.class);
		
		if(result == null) {
			System.out.println("No documents updated");
		} else {
			System.out.println(result.getModifiedCount() + " document(s) updated..");
		}*/
	}

	@Override
	public void insertDdHist(DBObject dbObject, String collection) {
		mongoTemplate.save(dbObject, collection);
	}
}
