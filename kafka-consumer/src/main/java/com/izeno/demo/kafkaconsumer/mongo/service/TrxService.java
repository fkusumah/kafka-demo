package com.izeno.demo.kafkaconsumer.mongo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.izeno.demo.kafkaconsumer.mongo.model.TrxHist;
import com.izeno.demo.kafkaconsumer.mongo.model.TrxMast;
import com.izeno.demo.kafkaconsumer.mongo.repository.CustomTrxRepository;
import com.izeno.demo.kafkaconsumer.mongo.repository.TrxHistRepository;
import com.izeno.demo.kafkaconsumer.mongo.repository.TrxMastRepository;
import com.mongodb.DBObject;

@Service
public class TrxService {

	@Autowired
	TrxHistRepository trxHistRepository;
	
	@Autowired
	TrxMastRepository trxMastRepository;
	
	@Autowired
	CustomTrxRepository customTrxRepository;
	
	//List<TrxHist> trxHistList = new ArrayList<TrxHist>();
	
	//CREATE
	/*public void createTrxHist(DdHist ddhist) {		
		String pattern = "dd-MM-yyyy";
		SimpleDateFormat dtf = new SimpleDateFormat(pattern);		
		
		trxHistRepository.save(new TrxHist(ddhist.tracct,ddhist.trbr, dtf.format(ddhist.trdate), ddhist.trdat6,ddhist.trdorc,ddhist.trancd,ddhist.amt, "", "", "", "", "", "", "", "", "", "", "", "", "", "", ddhist.year,ddhist.month,ddhist.day, 0, 0));				
	}*/	
	
	//CREATE
	public void createTrxHist(TrxHist trxHist) {
		trxHistRepository.save(trxHist);		
	}
	
	public void createTrxHist(DBObject dbObject, String collection) {
		customTrxRepository.insertDdHist(dbObject, "TRX_HIST");
	}
	
	/*public void showAllTrx() {		 
		trxHistList = trxHistRepository.findAll(); 
		trxHistList.forEach(item -> System.out.println(item));
	}*/
	
	public TrxMast getTrxMastByAcctNo(String acctno) {
		 TrxMast trxMast = trxMastRepository.findTrxMastByAcctNo(acctno);
		 return trxMast;
	}
	
	//UPDATE cbal
	public void updateCbal(String acctno, int cbal) {
		customTrxRepository.updateCbal(acctno, cbal);
	 }
}