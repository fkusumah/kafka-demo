package com.izeno.demo.kafkaconsumer.arctype;

import org.apache.kafka.common.serialization.Deserializer;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serializer;

import com.izeno.demo.kafkaconsumer.model.TradeModel;

public class TradeSerde implements Serde<TradeModel>{

	@Override
	public Serializer<TradeModel> serializer() {		
		return new JsonSerializer<TradeModel>();
	}

	@Override
	public Deserializer<TradeModel> deserializer() {
		return new JsonDeserializer<TradeModel>();
	}

}
