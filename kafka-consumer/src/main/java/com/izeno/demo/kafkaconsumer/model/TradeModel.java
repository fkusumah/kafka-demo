package com.izeno.demo.kafkaconsumer.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TradeModel {
    public Integer id;
    public String ticker;
    public Integer price;
    public Integer quantity;
    public Integer avg;
    
    public String toString() { 
        return "Id: '" + this.id + "', Ticker: '" + this.ticker + "', Price: '" + this.price + "'Quantity: '" + this.quantity + "'Avg: '" + this.avg + "'";
    }
}