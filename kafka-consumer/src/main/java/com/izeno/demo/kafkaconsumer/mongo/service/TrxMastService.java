package com.izeno.demo.kafkaconsumer.mongo.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.izeno.demo.kafkaconsumer.mongo.model.TrxMast;
import com.izeno.demo.kafkaconsumer.mongo.repository.CustomTrxRepository;
import com.izeno.demo.kafkaconsumer.mongo.repository.TrxMastRepository;

@Service
public class TrxMastService {
	
	@Autowired
	TrxMastRepository trxMastRepository;
	
	@Autowired
	CustomTrxRepository customTrxMastRepository;
	
	List<TrxMast> trxMastList = new ArrayList<TrxMast>();
	
	//CREATE
	public void createTrxMast() {		
		
		trxMastRepository.save(new TrxMast("16","10001","S","Colorado William",789, "", "", "", "", "", "", "", "", 0, "", "", "", "", "", "", "", "", "", "", "", ""));
		trxMastRepository.save(new TrxMast("7681","10002","S","Ronan Flynn",716, "", "", "", "", "", "", "", "", 10000, "", "", "", "", "", "", "", "", "", "", "", ""));
		trxMastRepository.save(new TrxMast("16","10003","S","Samson Lucas",270, "", "", "", "", "", "", "", "", 20000, "", "", "", "", "", "", "", "", "", "", "", ""));				
	}
	
	public void showAllTrx() {		 
		trxMastList = trxMastRepository.findAll(); 
		trxMastList.forEach(item -> System.out.println(item));
	}
	
	//UPDATE cbal
	public void updateCbal(String acctno, int cbal) {
		 //System.out.println("Updating cbal for " + acctno);
		 customTrxMastRepository.updateCbal(acctno, cbal);
	 }
	
	public TrxMast getTrxMastByAcctNo(String acctno) {
		 //System.out.println("Getting ddmast by acctno: " + acctno);
		 TrxMast trxMast = trxMastRepository.findTrxMastByAcctNo(acctno);
		 return trxMast;
	}
	
	public TrxMast getTrxMastByCifNo(String cifno) {
		 //System.out.println("Getting ddmast by cifno: " + cifno);
		 TrxMast trxMast = trxMastRepository.findTrxMastByAcctNo(cifno);
		 return trxMast;
	}
}
