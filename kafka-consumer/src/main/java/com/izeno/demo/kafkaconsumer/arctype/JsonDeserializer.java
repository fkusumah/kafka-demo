package com.izeno.demo.kafkaconsumer.arctype;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.kafka.common.errors.SerializationException;
import org.apache.kafka.common.serialization.Deserializer;

import java.util.Map;

public class JsonDeserializer<T> implements Deserializer<T> {
    private ObjectMapper objectMapper = new ObjectMapper();
    private Class<T> clazz;

    @SuppressWarnings("unchecked")
    @Override
    public void configure(Map<String, ?> props, boolean isKey) {
        clazz = (Class<T>) props.get("Class");
    }

    @Override
    public void close() { }

    @Override
    public T deserialize(String topic, byte[] bytes) {
        if (bytes == null)
            return null;
              
        T data = null;
        
        try {
        	Map record = objectMapper.readValue(new String(bytes), Map.class);
			Map payload = (Map) record.get("payload");
			Map after =  (Map) payload.get("after");
			
            if (after != null) {
            	// Incoming from Debezium
            	data = objectMapper.readValue(objectMapper.writeValueAsBytes(after), clazz);    			
            }

        } catch (Exception e) {
            throw new SerializationException(e);
        }
        return data;
    }
}