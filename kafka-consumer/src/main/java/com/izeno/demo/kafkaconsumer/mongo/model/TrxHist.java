package com.izeno.demo.kafkaconsumer.mongo.model;

import org.springframework.data.mongodb.core.mapping.Document;

@Document("TRX_HIST")
public class TrxHist {

	public String tracct; 
	public String trbr;
	public String trdate; 
	public String trdat6; 
	public String trdorc; 
	public String trancd; 
	public Integer amt; 
	public String auxtrx;
	public String trremk; 
	public String eftacc; 
	public String treffd; 
	public String treff6; 
	public String truser; 
	public String tratyp; 
	public String seq;    
	public String serial; 
	public String tlbds1; 
	public String tlbds2; 
	public String trstat; 
	public String timent; 
	public String dorc;   
	public Integer year;  
	public Integer month;  
	public Integer day;    	
	public Integer endBalance;
	public Integer startBalance;
	
	public TrxHist() {
	}
	
	public TrxHist(String tracct, String trbr, String trdate, String rdat6, String trdorc, String trancd, Integer amt,
			String auxtrx, String trremk, String eftacc, String treffd, String treff6, String truser, String tratyp,
			String seq, String serial, String tlbds1, String tlbds2, String trstat, String timent, String dorc,
			Integer year, Integer month, Integer day, Integer endBalance, Integer startBalance) {
		super();
		this.tracct = tracct;
		this.trbr = trbr;
		this.trdate = trdate;
		this.trdat6 = rdat6;
		this.trdorc = trdorc;
		this.trancd = trancd;
		this.amt = amt;
		this.auxtrx = auxtrx;
		this.trremk = trremk;
		this.eftacc = eftacc;
		this.treffd = treffd;
		this.treff6 = treff6;
		this.truser = truser;
		this.tratyp = tratyp;
		this.seq = seq;
		this.serial = serial;
		this.tlbds1 = tlbds1;
		this.tlbds2 = tlbds2;
		this.trstat = trstat;
		this.timent = timent;
		this.dorc = dorc;
		this.year = year;
		this.month = month;
		this.day = day;
		this.endBalance = endBalance;
		this.startBalance = startBalance;
	}
	public String getTracct() {
		return tracct;
	}
	public void setTracct(String tracct) {
		this.tracct = tracct;
	}
	public String getTrbr() {
		return trbr;
	}
	public void setTrbr(String trbr) {
		this.trbr = trbr;
	}
	public String getTrdate() {
		return trdate;
	}
	public void setTrdate(String trdate) {
		this.trdate = trdate;
	}
	public String getTrdat6() {
		return trdat6;
	}
	public void setTrdat6(String trdat6) {
		this.trdat6 = trdat6;
	}
	public String getTrdorc() {
		return trdorc;
	}
	public void setTrdorc(String trdorc) {
		this.trdorc = trdorc;
	}
	public String getTrancd() {
		return trancd;
	}
	public void setTrancd(String trancd) {
		this.trancd = trancd;
	}
	public Integer getAmt() {
		return amt;
	}
	public void setAmt(Integer amt) {
		this.amt = amt;
	}
	public String getAuxtrx() {
		return auxtrx;
	}
	public void setAuxtrx(String auxtrx) {
		this.auxtrx = auxtrx;
	}
	public String getTrremk() {
		return trremk;
	}
	public void setTrremk(String trremk) {
		this.trremk = trremk;
	}
	public String getEftacc() {
		return eftacc;
	}
	public void setEftacc(String eftacc) {
		this.eftacc = eftacc;
	}
	public String getTreffd() {
		return treffd;
	}
	public void setTreffd(String treffd) {
		this.treffd = treffd;
	}
	public String getTreff6() {
		return treff6;
	}
	public void setTreff6(String treff6) {
		this.treff6 = treff6;
	}
	public String getTruser() {
		return truser;
	}
	public void setTruser(String truser) {
		this.truser = truser;
	}
	public String getTratyp() {
		return tratyp;
	}
	public void setTratyp(String tratyp) {
		this.tratyp = tratyp;
	}
	public String getSeq() {
		return seq;
	}
	public void setSeq(String seq) {
		this.seq = seq;
	}
	public String getSerial() {
		return serial;
	}
	public void setSerial(String serial) {
		this.serial = serial;
	}
	public String getTlbds1() {
		return tlbds1;
	}
	public void setTlbds1(String tlbds1) {
		this.tlbds1 = tlbds1;
	}
	public String getTlbds2() {
		return tlbds2;
	}
	public void setTlbds2(String tlbds2) {
		this.tlbds2 = tlbds2;
	}
	public String getTrstat() {
		return trstat;
	}
	public void setTrstat(String trstat) {
		this.trstat = trstat;
	}
	public String getTiment() {
		return timent;
	}
	public void setTiment(String timent) {
		this.timent = timent;
	}
	public String getDorc() {
		return dorc;
	}
	public void setDorc(String dorc) {
		this.dorc = dorc;
	}
	public Integer getYear() {
		return year;
	}
	public void setYear(Integer year) {
		this.year = year;
	}
	public Integer getMonth() {
		return month;
	}
	public void setMonth(Integer month) {
		this.month = month;
	}
	public Integer getDay() {
		return day;
	}
	public void setDay(Integer day) {
		this.day = day;
	}
	public Integer getEndBalance() {
		return endBalance;
	}
	public void setEndBalance(Integer endBalance) {
		this.endBalance = endBalance;
	}
	public Integer getStartBalance() {
		return startBalance;
	}
	public void setStartBalance(Integer startBalance) {
		this.startBalance = startBalance;
	}

	@Override
	public String toString() {
		return "TrxHist [tracct=" + tracct + ", trbr=" + trbr + ", trdate=" + trdate + ", trdat6=" + trdat6
				+ ", trdorc=" + trdorc + ", trancd=" + trancd + ", amt=" + amt + ", auxtrx=" + auxtrx + ", trremk="
				+ trremk + ", eftacc=" + eftacc + ", treffd=" + treffd + ", treff6=" + treff6 + ", truser=" + truser
				+ ", tratyp=" + tratyp + ", seq=" + seq + ", serial=" + serial + ", tlbds1=" + tlbds1 + ", tlbds2="
				+ tlbds2 + ", trstat=" + trstat + ", timent=" + timent + ", dorc=" + dorc + ", year=" + year
				+ ", month=" + month + ", day=" + day + ", endBalance=" + endBalance + ", startBalance=" + startBalance
				+ "]";
	}
}
