package com.izeno.demo.kafkaconsumer.messaging;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.izeno.demo.kafkaconsumer.mongo.model.TrxMast;
import com.izeno.demo.kafkaconsumer.mongo.repository.CustomTrxRepository;
import com.izeno.demo.kafkaconsumer.mongo.service.TrxService;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;

@Component
public class Consumer {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(Consumer.class);
	private ObjectMapper objectMapper = new ObjectMapper();
	
	private int count = 1;
	
	@Autowired
	TrxService trxService;
	
	@Autowired
	CustomTrxRepository customTrxRepository;
	
	@KafkaListener(topics = "trx_hist", containerFactory = "kafkaListenerContainerFactory")
	public void processMessage(String content){
		
		
		//LOGGER.info("Incoming data {}-> {}", "trx_hist", count);
		//count++;
		
		try {
			Map<String, Object> record = objectMapper.readValue(content, new TypeReference<Map<String, Object>>() {});
			String tracct = String.valueOf(record.get("tracct"));
			String trdorc = String.valueOf(record.get("trdorc"));
			int amt = (int) record.get("amt");
			
			TrxMast trxMast = trxService.getTrxMastByAcctNo(tracct);
			
			if(trxMast != null) {
				
				int startBalance = trxMast.getCbal();
            	int endBalance = 0;
            	
            	if(trdorc.equalsIgnoreCase("C")) {
            		endBalance = startBalance + amt;
            	} else if(trdorc.equalsIgnoreCase("D")) {
            		endBalance = startBalance - amt;
            	}
            	
            	record.put("start_balance", startBalance);
            	record.put("end_balance", endBalance);
				
				DBObject dbObject = new BasicDBObject(record);
				trxService.createTrxHist(dbObject, "TRX_HIST");
				
				trxService.updateCbal(tracct, endBalance);
           
            	//LOGGER.info("Incoming data {}-> {}", "trx_hist", tracct+"|"+trdorc+"|"+amt);
				LOGGER.info("Incoming data {}-> {}", "trx_hist", count);
				count++;
			} else {
				LOGGER.info("Incoming data {}-> {}", "trx_hist", tracct+" - not found in ddmast");
			}
			
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
	}
	
	
	/*@KafkaListener(topics = "mysqlserver1.edm_demo.ddhist_kafka", containerFactory = "kafkaListenerContainerFactory")
    public void processMessage(String content){
				
		try {
			Map<String, Object> record = objectMapper.readValue(content, new TypeReference<Map<String, Object>>() {});
			Map<String, Object> payload = (Map<String, Object>) record.get("payload");			
			Map<String, Object> after =  (Map<String, Object>) payload.get("after");
			
			if (after != null) {
				// Incoming from Debezium
            	//DdHist ddhist = objectMapper.readValue(objectMapper.writeValueAsBytes(after), DdHist.class);
				String tracct = String.valueOf(after.get("tracct"));
				String jsonAfter = objectMapper.writeValueAsString(after);
				producer.publishToTopic(tracct, jsonAfter);
				
				String trdorc = String.valueOf(after.get("trdorc"));
				int amt = (int) after.get("amt");
				
				TrxMast trxMast = trxMastService.getTrxMastByAcctNo(tracct);
				
				if(trxMast != null) {
					
					int startBalance = trxMast.getCbal();
	            	int endBalance = 0;
	            	
	            	if(trdorc.equalsIgnoreCase("C")) {
	            		endBalance = startBalance + amt;
	            	} else if(trdorc.equalsIgnoreCase("D")) {
	            		endBalance = startBalance - amt;
	            	}
	            	
	            	after.put("start_balance", startBalance);
	            	after.put("end_balance", endBalance);
					
					DBObject dbObject = new BasicDBObject(after);
					customTrxRepository.insertDdHist(dbObject, "TRX_HIST");
					
					trxMastService.updateCbal(tracct, endBalance);
	           
	            	LOGGER.info("Incoming data {}-> {}", "dbserver1.edm_demo.ddhist", tracct);
				} else {
					LOGGER.info("Incoming data {}-> {}", "dbserver1.edm_demo.ddhist", tracct+" - not found in ddmast");
				}
			}
			
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
	}*/
	
	/*@KafkaListener(topics = "dbserver1.edm_demo.ddhist", groupId = "test-consumer-group")
    public void processMessage(String content){
		//String msg = "Masuk";
		//LOGGER.info("Incoming data {}-> {}", "dbserver1.edm_demo.ddhist", msg);
		
		try {
			Map<String, Object> record = objectMapper.readValue(content, new TypeReference<Map<String, Object>>() {});			
			Map<String, Object> payload = (Map<String, Object>) record.get("payload");			
			Map<String, Object> after =  (Map<String, Object>) payload.get("after");
			
			if (after != null) {
				
				// Incoming from Debezium
            	//DdHist ddhist = objectMapper.readValue(objectMapper.writeValueAsBytes(after), DdHist.class);
				String tracct = String.valueOf(after.get("tracct"));
				String trdorc = String.valueOf(after.get("trdorc"));
				int amt = (int) after.get("amt");
            	
            	TrxMast trxMast = trxMastService.getTrxMastByAcctNo(tracct);
            	
            	int startBalance = trxMast.getCbal();
            	int endBalance = 0;
            	
            	if(trdorc.equalsIgnoreCase("C")) {
            		endBalance = startBalance + amt;
            	} else if(trdorc.equalsIgnoreCase("D")) {
            		endBalance = startBalance - amt;
            	}
            	
            	after.put("start_balance", startBalance);
            	after.put("end_balance", endBalance);
				
				DBObject dbObject = new BasicDBObject(after);
				customTrxRepository.insertDdHist(dbObject, "TRX_HIST");
				
				trxMastService.updateCbal(tracct, endBalance);
				LOGGER.info("Incoming data {}-> {}", "dbserver1.edm_demo.ddhist", tracct);
			}else {
				LOGGER.info("Incoming data {}-> {}", "dbserver1.edm_demo.ddhist", "Other Message");
			}					
			
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}*/
	
	/*@KafkaListener(topics = "dbserver1.edm_demo.ddhist", groupId = "test-consumer-group")
    public void processMessage(String content){
        LOGGER.info("Incoming data {}-> {}", "dbserver1.edm_demo.ddhist", content);
        
        //String pattern = "yyyy-MM-dd";
		//SimpleDateFormat dtf = new SimpleDateFormat(pattern);
        
        try {
			Map record = objectMapper.readValue(content, Map.class);
			Map payload = (Map) record.get("payload");
			Map after =  (Map) payload.get("after");
			
            if (after != null) {
            	// Incoming from Debezium
            	DdHist ddhist = objectMapper.readValue(objectMapper.writeValueAsBytes(after), DdHist.class);
            	
            	TrxMast trxMast = trxMastService.getTrxMastByAcctNo(ddhist.tracct);
            	
            	int startBalance = trxMast.getCbal();
            	int endBalance = 0;
            	
            	if(ddhist.trdorc.equalsIgnoreCase("C")) {
            		endBalance = startBalance + ddhist.amt;
            	} else if(ddhist.trdorc.equalsIgnoreCase("D")) {
            		endBalance = startBalance - ddhist.amt;
            	}
            	
            	String trDate = ddhist.year + "-" + ddhist.month + "-" + ddhist.day;
            	TrxHist trxHist = new TrxHist(ddhist.tracct,ddhist.trbr, trDate, ddhist.trdat6,ddhist.trdorc,ddhist.trancd,ddhist.amt, "", "", "", "", "", "", "", "", "", "", "", "", "", "", ddhist.year,ddhist.month,ddhist.day, endBalance, startBalance);
            
            	trxHistService.createTrxHist(trxHist);
            	trxMastService.updateCbal(ddhist.tracct, endBalance);
            	            	
    			LOGGER.info("Incoming ddhist {}-> {}", "ddhist", ddhist);
            }
			
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }*/
	
	/*@KafkaListener(topics = "dbserver1.dbo.ddmast", groupId = "test-consumer-group")
    public void processMessage(String content){
		LOGGER.info("Incoming data {}-> {}", "dbserver1.dbo.ddmast", content);
	}*/
	
    /*@KafkaListener(topics = "dbserver1.edm_demo.trade", groupId = "test-consumer-group")
    public void processMessage(String content){
        LOGGER.info("Incoming data {}-> {}", "dbserver1.edm_demo.trade", content);
        
        try {
			Map record = objectMapper.readValue(content, Map.class);
			Map payload = (Map) record.get("payload");
			Map after =  (Map) payload.get("after");
			
            if (after != null) {
            	// Incoming from Debezium
            	TradeModel data = objectMapper.readValue(objectMapper.writeValueAsBytes(after), TradeModel.class);
    			LOGGER.info("Incoming Trade {}-> {}", "Trade", data);
            }
			
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }*/
	
	/*@KafkaListener(topics = "dbserver1.edm_demo.ddhist", groupId = "test-consumer-group")
	  public void consume(ConsumerRecord<String, DdHist> record) {
		System.out.println(String.format("Consumed message -> %s", record.value().getBefore().getAmt()));
	  }*/
	
	/*@KafkaListener(topics = "dbserver1.edm_demo.trade", groupId = "test-consumer-group", containerFactory = "kafkaListenerContainerFactory")
	  public void consume(ConsumerRecord<String, TradeModel> record) {
		LOGGER.info("Incoming quote {}-> {}", "dbserver1.edm_demo.trade", record.value());
	  }*/
	
	/*@KafkaListener(topics = "dbserver1.edm_demo.trade", groupId = "test-consumer-group", containerFactory = "kafkaListenerContainerFactory")
	  public void consume(@Payload TradeModel record) {
		LOGGER.info("Incoming quote {}-> {}", "dbserver1.edm_demo.trade", record);
	  }*/
}