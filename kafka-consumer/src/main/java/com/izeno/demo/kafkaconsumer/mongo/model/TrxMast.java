package com.izeno.demo.kafkaconsumer.mongo.model;

import org.springframework.data.mongodb.core.mapping.Document;

@Document("ddmast")
public class TrxMast {
	public String branch;
	public String acctno;
	public String actype;
	public String sname;
	public int cifno;
	public String officr;
	public String status;
	public String fclass;
	public String ddctyp;
	public String ddgrup;
	public String datop7;
	public String datst7;
	public String hold;
	public int cbal;
	public String accrue;
	public String ytdint;
	public String lintpd;
	public String sccode;
	public String nodrc;
	public String amtdrc;
	public String nocrc;
	public String amtcrc;
	public String rate;
	public String revdt7;
	public String term;
	public String nplflag;
	
	public TrxMast() {
	}

	public TrxMast(String branch, String acctno, String actype, String sname, int cifno, String officr, String status,
			String fclass, String ddctyp, String ddgrup, String datop7, String datst7, String hold, int cbal,
			String accrue, String ytdint, String lintpd, String sccode, String nodrc, String amtdrc, String nocrc,
			String amtcrc, String rate, String revdt7, String term, String nplflag) {
		super();
		this.branch = branch;
		this.acctno = acctno;
		this.actype = actype;
		this.sname = sname;
		this.cifno = cifno;
		this.officr = officr;
		this.status = status;
		this.fclass = fclass;
		this.ddctyp = ddctyp;
		this.ddgrup = ddgrup;
		this.datop7 = datop7;
		this.datst7 = datst7;
		this.hold = hold;
		this.cbal = cbal;
		this.accrue = accrue;
		this.ytdint = ytdint;
		this.lintpd = lintpd;
		this.sccode = sccode;
		this.nodrc = nodrc;
		this.amtdrc = amtdrc;
		this.nocrc = nocrc;
		this.amtcrc = amtcrc;
		this.rate = rate;
		this.revdt7 = revdt7;
		this.term = term;
		this.nplflag = nplflag;
	}

	public String getBranch() {
		return branch;
	}

	public void setBranch(String branch) {
		this.branch = branch;
	}

	public String getAcctno() {
		return acctno;
	}

	public void setAcctno(String acctno) {
		this.acctno = acctno;
	}

	public String getActype() {
		return actype;
	}

	public void setActype(String actype) {
		this.actype = actype;
	}

	public String getSname() {
		return sname;
	}

	public void setSname(String sname) {
		this.sname = sname;
	}

	public int getCifno() {
		return cifno;
	}

	public void setCifno(int cifno) {
		this.cifno = cifno;
	}

	public String getOfficr() {
		return officr;
	}

	public void setOfficr(String officr) {
		this.officr = officr;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getFclass() {
		return fclass;
	}

	public void setFclass(String fclass) {
		this.fclass = fclass;
	}

	public String getDdctyp() {
		return ddctyp;
	}

	public void setDdctyp(String ddctyp) {
		this.ddctyp = ddctyp;
	}

	public String getDdgrup() {
		return ddgrup;
	}

	public void setDdgrup(String ddgrup) {
		this.ddgrup = ddgrup;
	}

	public String getDatop7() {
		return datop7;
	}

	public void setDatop7(String datop7) {
		this.datop7 = datop7;
	}

	public String getDatst7() {
		return datst7;
	}

	public void setDatst7(String datst7) {
		this.datst7 = datst7;
	}

	public String getHold() {
		return hold;
	}

	public void setHold(String hold) {
		this.hold = hold;
	}

	public int getCbal() {
		return cbal;
	}

	public void setCbal(int cbal) {
		this.cbal = cbal;
	}

	public String getAccrue() {
		return accrue;
	}

	public void setAccrue(String accrue) {
		this.accrue = accrue;
	}

	public String getYtdint() {
		return ytdint;
	}

	public void setYtdint(String ytdint) {
		this.ytdint = ytdint;
	}

	public String getLintpd() {
		return lintpd;
	}

	public void setLintpd(String lintpd) {
		this.lintpd = lintpd;
	}

	public String getSccode() {
		return sccode;
	}

	public void setSccode(String sccode) {
		this.sccode = sccode;
	}

	public String getNodrc() {
		return nodrc;
	}

	public void setNodrc(String nodrc) {
		this.nodrc = nodrc;
	}

	public String getAmtdrc() {
		return amtdrc;
	}

	public void setAmtdrc(String amtdrc) {
		this.amtdrc = amtdrc;
	}

	public String getNocrc() {
		return nocrc;
	}

	public void setNocrc(String nocrc) {
		this.nocrc = nocrc;
	}

	public String getAmtcrc() {
		return amtcrc;
	}

	public void setAmtcrc(String amtcrc) {
		this.amtcrc = amtcrc;
	}

	public String getRate() {
		return rate;
	}

	public void setRate(String rate) {
		this.rate = rate;
	}

	public String getRevdt7() {
		return revdt7;
	}

	public void setRevdt7(String revdt7) {
		this.revdt7 = revdt7;
	}

	public String getTerm() {
		return term;
	}

	public void setTerm(String term) {
		this.term = term;
	}

	public String getNplflag() {
		return nplflag;
	}

	public void setNplflag(String nplflag) {
		this.nplflag = nplflag;
	}

	@Override
	public String toString() {
		return "TrxMast [branch=" + branch + ", acctno=" + acctno + ", actype=" + actype + ", sname=" + sname
				+ ", cifno=" + cifno + ", officr=" + officr + ", status=" + status + ", fclass=" + fclass + ", ddctyp="
				+ ddctyp + ", ddgrup=" + ddgrup + ", datop7=" + datop7 + ", datst7=" + datst7 + ", hold=" + hold
				+ ", cbal=" + cbal + ", accrue=" + accrue + ", ytdint=" + ytdint + ", lintpd=" + lintpd + ", sccode="
				+ sccode + ", nodrc=" + nodrc + ", amtdrc=" + amtdrc + ", nocrc=" + nocrc + ", amtcrc=" + amtcrc
				+ ", rate=" + rate + ", revdt7=" + revdt7 + ", term=" + term + ", nplflag=" + nplflag + "]";
	}	
}
