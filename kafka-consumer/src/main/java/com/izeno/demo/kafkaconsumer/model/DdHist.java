package com.izeno.demo.kafkaconsumer.model;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class DdHist {

	public String tracct; 
	public String trbr;
	public Date trdate; 
	public String trdat6; 
	public String trdorc; 
	public String trancd; 
	public Integer amt; 
	public String auxtrx;
	public String trremk; 
	public String eftacc; 
	public String treffd; 
	public String treff6; 
	public String truser; 
	public String tratyp; 
	public String seq;    
	public String serial; 
	public String tlbds1; 
	public String tlbds2; 
	public String trstat; 
	public String timent; 
	public String dorc;   
	public Integer year;  
	public Integer month;  
	public Integer day;	
	
	@Override
	public String toString() {
		return "DdHist [tracct=" + tracct + ", trbr=" + trbr + ", trdate=" + trdate + ", trdat6=" + trdat6 + ", trdorc="
				+ trdorc + ", trancd=" + trancd + ", amt=" + amt + ", auxtrx=" + auxtrx + ", trremk=" + trremk
				+ ", eftacc=" + eftacc + ", treffd=" + treffd + ", treff6=" + treff6 + ", truser=" + truser
				+ ", tratyp=" + tratyp + ", seq=" + seq + ", serial=" + serial + ", tlbds1=" + tlbds1 + ", tlbds2="
				+ tlbds2 + ", trstat=" + trstat + ", timent=" + timent + ", dorc=" + dorc + ", year=" + year
				+ ", month=" + month + ", day=" + day
				+ "]";
	}
	
	
}
