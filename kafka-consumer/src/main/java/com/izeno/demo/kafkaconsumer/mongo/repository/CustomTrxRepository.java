package com.izeno.demo.kafkaconsumer.mongo.repository;

import com.mongodb.DBObject;

public interface CustomTrxRepository {

	void updateCbal(String acctno, int cbal);
	void insertDdHist(DBObject dbObject, String collection);
}
