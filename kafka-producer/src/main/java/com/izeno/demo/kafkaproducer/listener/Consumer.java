package com.izeno.demo.kafkaproducer.listener;

import java.text.SimpleDateFormat;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.izeno.demo.kafkaproducer.service.Producer;

@Component
public class Consumer {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(Consumer.class);
	private ObjectMapper objectMapper = new ObjectMapper();
	
	private AtomicInteger count = new AtomicInteger(1);	
	
	private AtomicLong ts = new AtomicLong(System.nanoTime());
	
	private int trxCount = (System.getenv("TRX_COUNT") != null) ? Integer.valueOf(System.getenv("TRX_COUNT")) : 4000;
	
	@Autowired
	Producer producer;
	
	@SuppressWarnings("unchecked")
	@KafkaListener(topics = "mysqlserver1.edm_demo.ddhist_kafka", containerFactory = "kafkaListenerContainerFactory")
    public void processMessage(String content){
				
		try {
			Map<String, Object> record = objectMapper.readValue(content, new TypeReference<Map<String, Object>>() {});
			Map<String, Object> payload = (Map<String, Object>) record.get("payload");			
			Map<String, Object> after =  (Map<String, Object>) payload.get("after");
			
			if (after != null) {
				// Incoming from Debezium
            	String tracct = String.valueOf(after.get("tracct"));
				String jsonAfter = objectMapper.writeValueAsString(after);
				//Thread.sleep(1);
				producer.publishToTopic(tracct, jsonAfter);
				int tmp = count.incrementAndGet();
				long tsPrev = ts.get();
				long tsNow = System.nanoTime();				
				
				if((tsNow - tsPrev) >= 1000000000) {
					LOGGER.info("Incoming data {}-> {}", "dbserver1.edm_demo.ddhist_kafka", tsPrev+"|"+tsNow+"|"+tmp+"|"+trxCount+"|"+count.get()+"|"+ts.get()+"|"+(tsNow - tsPrev));
					ts.setPlain(tsNow);					
					if(tmp >= trxCount) {
						count.set(1);
						Thread.sleep(2000);
					}
				}
				//LOGGER.info("Incoming data {}-> {}", "dbserver1.edm_demo.ddhist_kafka", tracct);
			}
			
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}
}