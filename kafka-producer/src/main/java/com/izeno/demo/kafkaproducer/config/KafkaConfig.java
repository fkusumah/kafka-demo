package com.izeno.demo.kafkaproducer.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.kafka.KafkaProperties;
import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.config.KafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.listener.ConcurrentMessageListenerContainer;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;

//@Configuration
//@EnableKafka
public class KafkaConfig {

	//private static final Logger LOGGER= LoggerFactory.getLogger(KafkaConfig.class);
	
	@Autowired
    private KafkaProperties kafkaProperties;		
       
	/**
     * Kafka ConsumerFactory configurations
     * @return
     */
    @Bean
    public ConsumerFactory<String, String> consumerFactory() {
    	return new DefaultKafkaConsumerFactory<>(kafkaProperties.buildConsumerProperties());
    }
    
    @Bean
    KafkaListenerContainerFactory<ConcurrentMessageListenerContainer<String, String>> kafkaListenerContainerFactory() {
        ConcurrentKafkaListenerContainerFactory<String, String> factory =
            new ConcurrentKafkaListenerContainerFactory<>();
        factory.setConsumerFactory(consumerFactory());
        factory.setConcurrency(5);
        factory.getContainerProperties().setPollTimeout(3000);
        return factory;
    }
}
