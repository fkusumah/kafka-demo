package com.izeno.demo.kafkaproducer.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class Producer {

	private static final Logger LOGGER = LoggerFactory.getLogger(Producer.class);
	
	public static final String TOPIC = "trx_hist";
	
	@Autowired
	private KafkaTemplate<String, String> kafkaTemp;
	
	public void publishToTopic(String key,String message) {
		//LOGGER.info("Incoming data {}-> {}", TOPIC, message);		
		this.kafkaTemp.send(TOPIC, key, message);
		//LOGGER.info("Incoming data {}-> {}", TOPIC, message);
	}
}